==================
Ceph Benchmarking
==================

1. Individual benchmarks
    * RADOS
        cd individual/rados
        ./ceph-bench-rados.sh   # runs all the rados tests, including parallel ones
        ./get-data.sh           # processes the output into data files for plotting
        ./plot-all.sh           # plots all the data files and saves the figures in fig-*
    * RBD
        cd individual/rbd 
        ./ceph-bench-rbd.sh     # runs all the block device tests
        ./get-data.sh           # processes the output into data files for plotting
        ./plot-all.sh           # plots all the data files and saves the figures in fig-*
    * RGW
        cd individual/rgw

2. Multisite asynchronous benchmarks

3. Multisite synchronous benchmarks

