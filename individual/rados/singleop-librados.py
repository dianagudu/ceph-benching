#!/usr/bin/env python

import rados, sys
import time

def test(nc, runno, op, sz):
    filename = "parallel%s-%s.librados.%s.%sKB.1th.log" % (runno,nc,op,sz)
    f = open(filename, "w")

    with rados.Rados(conffile='/etc/ceph/ceph.conf') as cluster:
        with cluster.open_ioctx('bench-2') as ioctx:
            size = int(sz) * 1024  # in Bytes
            data = '0' * size

            total_wr = 1024**3
            total_mb = 1024

            count = int(total_wr / size)

            start = time.clock()
            for i in range(0, count):
                obj = "obj_librados_bench_%s_%s" % (sz,i)
                if op == 'write':
                    ioctx.write_full(obj, data)
                else:
                    ioctx.read(obj, size)
            end = time.clock()

            string = "%s\n" % (total_mb / (end-start))
            f.write(string)
    f.close()

def print_usage():
    print ("[USAGE] python %s <nc> <runno> <op> <size>" % str(sys.argv[0]))
    print ("where \t nc is the number of parallel clients,")
    print ("\t runno is the run number,")
    print ("\t op in {read, write},")
    print ("\t size is the size of the objects to be written/read in KB")
    sys.exit(2)

def main():
    # check if the number of input arguments is correct
    total = len (sys.argv)
    if total != 5:
        print_usage()

    # get input arguments
    nc = sys.argv[1]        # number of parallel clients
    runno = sys.argv[2]     # run number
    op = sys.argv[3]        # operation: read or write
    sz = sys.argv[4]        # object size
    # check correctness of input arguments
    if not nc.isdigit():
        print ("[ERROR] Choose valid number of parallel clients!")
        print_usage()
    if not runno.isdigit():
        print ("[ERROR] Choose valid run number!")
        print_usage()
    if (op != "seq" and op != "write"):
        print ("[ERROR] Choose operation: seq or write!")
        print_usage()
    if not sz.isdigit():
        print "[ERROR] Provide a valid size in KB!"
        print_usage()
    # write
    test(nc,runno,op,sz)

if __name__ == '__main__':
    main()
