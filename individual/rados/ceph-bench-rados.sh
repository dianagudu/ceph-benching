#!/bin/bash

create_pool() {
    local rep=$1
    pg_num=$nPGs
    ceph osd pool create bench-${rep} ${pg_num} ${pg_num}
    ceph osd pool set bench-${rep} size ${rep}
}

create_pool_parallel() {
    local rep=$1
    local host=$2
    pg_num=$nPGs
    ceph osd pool create bench-${rep}-${host} ${pg_num} ${pg_num}
    ceph osd pool set bench-${rep}-${host} size ${rep}
}

create_pool_pg() {
    pg=$1
    ceph osd pool create bench-${pg} ${pg} ${pg}
}

delete_pool() {
    local rep=$1
    ceph osd pool delete bench-${rep} bench-${rep} --yes-i-really-really-mean-it
}

rados_bench() {
    local size=${1:?no object size given}
    local rep=${2:?no replication level given} # or #PGs in pgs_series test
    local threads=${3:-16}
    local size_bytes=$((size * 1024))
    local op=${4:?no operation given}
    local dur=$DURATION
    local run=$5

    # add --no-cleanup if we are writing
    extra=""
    if [ "$op" = "write" ]; then 
        extra="--no-cleanup"
        dur=$((DURATION * 2))
    fi
    # clear chaches
    ./../sync.sh
    # run benchmark
    echo "# rados size=$size threads=$threads rep=$rep op=$op run=$run"
    rados bench -p bench-${rep} ${dur} ${op} -b $size_bytes -t $threads $extra | \
      tee "$outdir/rados_bench${run}.${op}.${size}KB.${threads}th.${rep}rep.log"
}

parallel_rados() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local rep=$3                        # chosen replication level
    local op=${4:?no operation given}   # operation: write or seq
    local runno=$5
    local NH=$6
    local dur=$DURATION

    # add --no-cleanup if we are writing
    extra=""
    if [ "$op" = "write" ]; then 
        extra="--no-cleanup"
        dur=$((DURATION * 2))
    fi

    # clear chaches
    ./../sync.sh

    suffix="${op}.${size}KB.${threads}th.${rep}rep.log"

    # launch several parallel rados bench
    pssh -h <(head -${NH} hosts_parallel_rados) -t 0 -i \
      "rados bench -p bench-${rep}-\`hostname\` ${dur} ${op} -b $size_bytes -t $threads $extra | \
      tee parallel${runno}-${NH}.${suffix}"
    
    # wait for them to finish
    sleep 5

    # gather results from remote machines to local host
    for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rados)`; do
        outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
        scp root@${host}:parallel${runno}-${NH}.${suffix} ${outfile}
        # remove output files on remote machines
        ssh root@${host} "rm parallel${runno}-${NH}.*"
    done
}

parallel_librados() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local op=${2:?no operation given}   # operation: write or seq
    local runno=$3
    local NH=$4

    ./../sync.sh
    pssh -h <(head -${NH} hosts_parallel_rados) -t 0 -i \
        "python singleop-librados.py $NH $runno $op $size"

    suffix="librados.${op}.${size}KB.1th.log"
    for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rados)`; do
        outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
        scp root@${host}:parallel${runno}-${NH}.${suffix} ${outfile}
        # remove output files on remote machines
        ssh root@${host} "rm parallel${runno}-${NH}.*"
    done
}

rados_series() {
    # run tests with different number of threads, for fixed block size
    for th in ${THREADS[*]}; do
        for size in ${SIZES_AUX[*]}; do
            for rep in ${REP[*]}; do
                for run in `seq 0 $((nruns-1))`; do
                    create_pool $rep
                    rados_bench $size $rep $th 'write' $run
                    rados_bench $size $rep $th 'seq' $run
                    delete_pool $rep
                done
            done
        done
    done
    # run tests with different block sizes, for fixed number of threads
    for size in ${SIZES[*]}; do
        for th in ${THREADS_AUX[*]}; do
            for rep in ${REP[*]}; do
                for run in `seq 0 $((nruns-1))`; do
                    create_pool $rep
                    rados_bench $size $rep $th 'write' $run
                    rados_bench $size $rep $th 'seq' $run
                    delete_pool $rep
                done
            done
        done
    done
}

parallel_series() {
    # values for best performance
    size=4096
    rep=1 #2

    for th in ${THREADS_AUX[*]}; do
        for i in `seq 0 $((nruns-1))`; do
            for NH in ${NHOSTS[*]}; do
                for host in `awk -F'.' '{print $1}' <(head -${NH} hosts_parallel_rados)`; do
                    create_pool_parallel $rep $host
                done
                parallel_rados $size $th $rep 'write' $i $NH
                #parallel_rados $size $th $rep 'seq' $i $NH
                for host in `awk -F'.' '{print $1}' <(head -${NH} hosts_parallel_rados)`; do
                    delete_pool ${rep}-${host}
                done
            done
        done
    done
}

parallel_librados_series() {
    for host in `awk '{print $1}' <(cat hosts_parallel_rados)`; do
        scp singleop-librados.py root@$host:
    done

    size=4096
    rep=2
    for i in `seq 0 $((nruns-1))`; do
        for NH in ${NHOSTS[*]}; do
            create_pool $rep
            parallel_librados $size 'write' $i $NH
            parallel_librados $size 'read' $i $NH
            delete_pool $rep
        done
    done
}

pgs_series() {
    for pg in ${PGS[*]}; do
        for th in ${THREADS[*]}; do
            for size in ${SIZES_AUX[*]}; do
                create_pool_pg $pg
                rados_bench $size $pg $th 'write' 
                rados_bench $size $pg $th 'seq' 
                delete_pool $pg
            done
        done
        for size in ${SIZES[*]}; do
            for th in ${THREADS_AUX[*]}; do
                create_pool_pg $pg
                rados_bench $size $pg $th 'write' 
                rados_bench $size $pg $th 'seq' 
                delete_pool $pg
            done
        done
    done
}

# initialize the test scenario
. init.sh

#########################################################################################

usage="$(basename "$0") [-h] [-o outdir] [-n nruns] -b benchmark -- program to benchmark the RADOS performance

where:
    -h  show this help test
    -o  set the output directory (default: out)
    -n  set the number of benchmark runs (default: 5)
    -b  select the benchmark to be run (mandatory)

options for benchmarks: 
    serial      serial rados bench tests, with different object sizes and number of threads
    parallel    rados bench tests with multiple clients
    pgs         serial rados bench tests with different number of placement groups
    plibrados   parallel tests using the librados API

Other parameters can be set in init.sh
"    
while getopts ':ho:n:b:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        b)  bench=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ -z "$bench" ]
then
    echo "$usage" >&2
    exit 1
fi

mkdir -p $outdir

case "$bench" in
    serial)     rados_series;;
    parallel)   parallel_series;;
    pgs)        pgs_series;;
    plibrados)  parallel_librados_series;;
    *)          printf "invalid benchmark name: -%s\n\n" "$bench" >&2
                echo "$usage" >&2
                ;;
esac

