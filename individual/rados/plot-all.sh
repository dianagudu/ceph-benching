#!/bin/bash

. init.sh

usage="$(basename "$0") [-h] [-d datdir] [-f figdir] [-n nruns] -b benchmark -- program to plot results for RADOS benchmarks

where:
    -h  show this help test
    -d  set the data directory where the data to be plotted are (default: dat)
    -f  set the figure directory where the plots will be saved (default: fig)
    -n  set the number of benchmark runs (default: 5)
    -b  select the benchmark to be plotted (mandatory)

options for benchmarks: 
    serial      serial rados bench tests, with different object sizes and number of threads
    parallel    rados bench tests with multiple clients
    pgs         serial rados bench tests with different number of placement groups
    plibrados   parallel tests using the librados API

Other parameters can be set in init.sh
"    
while getopts ':hd:f:n:b:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        d)  datdir=$OPTARG;;
        f)  figdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        b)  bench=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ -z "$bench" ]
then
    echo "$usage" >&2
    exit 1
fi

mkdir -p $figdir

case "$bench" in
    serial)     
        for op in 'write' 'seq'; do
            for y in 'bw' 'lat'; do
                for s in ${SIZES_AUX[*]}; do
                    python plot.py $datdir $figdir $op 'th' $y $s $nruns
                done
                for th in ${THREADS_AUX[*]}; do
                    python plot.py $datdir $figdir $op 'ws' $y $th $nruns
                done
            done
        done
        ;;
    parallel)   
        for th in 16 ; do #`seq 1 15`; do
            python plotpar.py $datdir $figdir 'bw' $th $size $rep $nruns 'rados-bench'
            python plotpar.py $datdir $figdir 'lat' $th $size $rep $nruns 'rados-bench'
        done
        ;;
    pgs)    
        for op in 'write' 'seq'; do
            for y in 'bw' 'lat'; do
                for s in ${SIZES_AUX[*]}; do
                    python plot-pgs.py $datdir $figdir $op 'th' $y $s $nruns
                done
                for th in ${THREADS_AUX[*]}; do
                    python plot-pgs.py $datdir $figdir $op 'ws' $y $th $nruns
                done
            done
        done
        ;;
    plibrados)
        python plotpar.py $datdir $figdir 'write' 'bw' 1 4096 2 $nruns "librados"
        ;;
    *)  printf "invalid benchmark name: -%s\n\n" "$bench" >&2
        echo "$usage" >&2
        ;;
esac


