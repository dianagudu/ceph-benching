#!/bin/bash

collect_data() {
    # get the necessary arguments
    op=$1     # read or write
    xval=$2   # th or ws
    yval=$3   # bw or lat
    rep=$4    # replication level OR #PGs
    run=$5    # the run number
    # pattern to search for in test results
    if [ "$yval" = 'bw' ]; then
        pattern="Bandwidth (MB"
        yheader="bandwidth(MB/s)"
    else
        pattern="Average Latency"
        yheader="latency(s)"
    fi
    # performance for different #threads
    if [ "$xval" = 'th' ]; then
        for ws in ${SIZES_AUX[*]}; do
            # get data for all replication levels 
                outfile="$datdir/data${run}-${op}-${yval}-${xval}-${ws}-${rep}rep.dat"
                echo "# threads ${yheader}" > $outfile
                for th in ${THREADS[*]}; do 
                    infile="${outdir}/rados_bench${run}.${op}.${ws}KB.${th}th.${rep}rep.log"
                    grep "${pattern}" ${infile} | awk "{print \"${th}\", \$3 }" | tee -a ${outfile}
                done
        done
    else # performance for different block sizes
        for th in ${THREADS_AUX[*]}; do
            # get data for all replication levels 
                outfile="$datdir/data${run}-${op}-${yval}-${xval}-${th}-${rep}rep.dat"
                echo "# block_size ${yheader}" > $outfile
                for ws in ${SIZES[*]}; do 
                    infile="${outdir}/rados_bench${run}.${op}.${ws}KB.${th}th.${rep}rep.log"
                    grep "${pattern}" ${infile} | awk "{print \"${ws}\", \$3 }" | tee -a ${outfile}
                done
        done
    fi
}

get_parallel_data() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local rep=$3                        # chosen replication level
    local op=${4:?no operation given}   # operation: write or seq
    local runno=$5

    datfile_bw=${datdir}/parallel${runno}-${op}-bw-${threads}-${size}-${rep}rep.dat
    datfile_lat=${datdir}/parallel${runno}-${op}-lat-${threads}-${size}-${rep}rep.dat
    suffix="${op}.${size}KB.${threads}th.${rep}rep.log"
    
    for NH in ${NHOSTS[*]}; do
        bw_avg=0
        lat_avg=0
        for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rados)`; do
            outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
            bw_avg=`grep "Bandwidth (MB" ${outfile} | awk '{print '$bw_avg'+$3 }'`
            lat_avg=`grep "Average Latency" ${outfile} | awk '{print '$lat_avg'+$3 }'`
        done
        #bw_avg=$(awk 'BEGIN { print '$bw_avg'/'$NH' }')
        lat_avg=$(awk 'BEGIN { print '$lat_avg'/'$NH' }')
        echo $NH ${bw_avg} | tee -a ${datfile_bw}
        echo $NH ${lat_avg} | tee -a ${datfile_lat}
    done
}

get_parallel_librados_data() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local op=${2:?no operation given}   # operation: write or seq
    local runno=$3

    datfile_bw=${datdir}/librados-parallel${runno}-${op}-bw-1-${size}-2rep.dat
    suffix="librados.${op}.${size}KB.1th.log"
    
    for NH in ${NHOSTS[*]}; do
        bw_agg=0
        for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rados)`; do
            outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
            aux=`cat $outfile`
            bw_agg=`awk 'BEGIN {print '$bw_agg'+'$aux' }'`
        done
        echo $NH ${bw_agg} | tee -a ${datfile_bw}
    done
}

get_all_rados_bench() {
    for rep in ${REP[*]}; do
        rm -rf ${datdir}/*.${rep}rep.dat
    done

    for op in 'write' 'seq'; do
        for xval in 'th' 'ws'; do
            for yval in 'bw' 'lat'; do
                for rep in ${REP[*]}; do
                    for i in `seq 0 $((nruns-1))`; do
                        collect_data $op $xval $yval $rep $i
                    done
                done
            done
        done
    done
}

get_all_parallel() {
    size=4096
    rep=1
    rm -rf ${datdir}/parallel*.dat
    for th in ${THREADS_AUX[*]}; do
        for i in `seq 0 $((nruns-1))`; do
            for op in 'write'; do # 'seq'; do
                get_parallel_data $size $th $rep $op $i
            done
        done
    done
}

get_all_parallel_librados() {
    rm -rf ${datdir}/librados*.dat
    for i in `seq 0 $((nruns-1))`; do
        for op in 'write' 'seq'; do
            get_parallel_librados_data 4096 $op $i
        done
    done
}

get_all_pgs_data() {
    for pg in ${PGS[*]}; do
        rm -rf ${datdir}/*.${pg}rep.dat
    done
    
    for pg in ${PGS[*]}; do
        for xval in 'th' 'ws'; do
            for yval in 'bw' 'lat'; do
                for op in 'write' 'seq'; do
                    for i in `seq 0 $((nruns-1))`; do
                        collect_data $op $xval $yval $pg $i
                    done
                done
            done
        done
    done
}

. init.sh

#########################################################################################

usage="$(basename "$0") [-h] [-o outdir] [-n nruns] [-d datdir] -b benchmark -- program to process performance data for RADOS

where:
    -h  show this help test
    -o  set the output directory where data were collected (default: out)
    -d  set the data directory (default: dat)
    -n  set the number of benchmark runs (default: 5)
    -b  select the benchmark for which to process collected data (mandatory)

options for benchmarks: 
    serial      serial rados bench tests, with different object sizes and number of threads
    parallel    rados bench tests with multiple clients
    pgs         serial rados bench tests with different number of placement groups
    plibrados   parallel tests using the librados API

Other parameters can be set in init.sh
"    
while getopts ':hd:o:n:b:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        d)  datdir=$OPTARG;;
        b)  bench=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ -z "$bench" ]
then
    echo "$usage" >&2
    exit 1
fi

mkdir -p $datdir

case "$bench" in
    serial)     get_all_rados_bench;;
    parallel)   get_all_parallel;;
    pgs)        get_all_pgs_data;;
    plibrados)  get_all_parallel_librados;;
    *)          printf "invalid benchmark name: -%s\n\n" "$bench" >&2
                echo "$usage" >&2
                ;;
esac

