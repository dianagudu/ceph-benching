#!/usr/bin/env python

import numpy as np
import pylab
from pylab import *
import os

def plot(datadir, figdir):
    # set figure size
    fig = plt.figure(figsize=(10,8))
    # load data from file data_librd_sizes_1th.dat
    file = "%s/librados-write-bw-ws-1-2rep.dat" %(datadir)
    data = pylab.loadtxt(file)
    # plot
    pylab.plot( data[:,0], data[:,1], label='bw', marker='o')

    my_title = "librados write performance with different object sizes"
    my_xlabel = 'write size (KB)'
    my_ylabel = 'bandwidth (MB/s)'

    xscale('log', basex=2) # use logarithmic scale

    fontsize=18
    title(my_title, fontsize=fontsize*1.1)
    xlabel(my_xlabel, fontsize=fontsize)
    ylabel(my_ylabel, fontsize=fontsize)
    legend(loc='lower right')
    grid(True)

    # save to file
    out = "%s/librados-ws-bw.pdf" % (figdir)
    plt.savefig(out, dpi=90, format="pdf", orientation="landscape")
    #show()

def print_usage():
    print ("[USAGE] python %s <datadir> <figdir>" % str(sys.argv[0]))
    print ("where \t datadir is the path to the data files,")
    print ("\t figdir is the path where the plots will be saved")
    sys.exit(2)

def main():
    # check if the number of input arguments is correct
    total = len (sys.argv)
    if total != 3:
        print_usage()

    # get input arguments
    datadir = sys.argv[1]   # relative path to data files 
    figdir = sys.argv[2]    # relative path to data files 

    # check correctness of input arguments
    if not (os.path.isdir(datadir) and os.path.exists(datadir)):
        print ("[ERROR] Choose valid path for data files!")
        print_usage()
    if not (os.path.isdir(figdir) and os.path.exists(figdir)):
        print ("[ERROR] Choose valid path for plot files!")
        print_usage()
    # plot
    plot(datadir, figdir)

if __name__ == '__main__':
    main()
