#!/usr/bin/env python

import numpy as np
import pylab
from pylab import *
import os

reps = ['1', '2', '3']
labels = ['no replica', '1 replica', '2 replicas']
markers = ['o', '>', 's']

def plot(datadir, figdir, op, x, y, no, nruns):
    # set figure size
    fig = plt.figure(figsize=(10,8))

    for i in range(0, len(reps)):
        data = []
        for run in range(0, int(nruns)):
            # use data for all three replication levels
            file = "%s/data%d-%s-%s-%s-%s-%srep.dat" %(datadir,run,op,y,x,no,reps[i])
            # load data from file
            data.append(pylab.loadtxt(file))

        # x values
        xval = data[0][:,0]
        # compute average of runs
        avg = np.mean(data[:], axis=0)[:,1]
        # compute standard deviation
        stdev = np.std(data[:], axis=0)[:,1]
        # plot
        pylab.plot( xval, avg, label=labels[i], marker=markers[i])
        # plot error bars
        pylab.errorbar( xval, avg, yerr=stdev, linestyle="None", marker="None")

    # different settings depending on input
    if x == 'th':   # measurements for different #threads
        my_xlabel = '# threads'
        my_title = "Rados %s performance with different number \n of threads, %s KB block size" %(op,no)
    else:           # measurements for different block sizes
        my_xlabel = 'block size (KB)'
        my_title = "Rados %s performance with different\n block sizes, %s threads" %(op,no)
        xscale('log', basex=2) # use logarithmic scale

    if y == 'bw':   # measured bandwidth
        my_ylabel = 'bandwidth (MB/s)'
        legend(loc='lower right')
    else:           # measured latency
        my_ylabel = 'latency (s)'
        legend(loc='upper left')

    fontsize=18
    title(my_title, fontsize=fontsize*1.1)  # figure title
    xlabel(my_xlabel, fontsize=fontsize)    # x asis labe;
    ylabel(my_ylabel, fontsize=fontsize)    # y axis label
    grid(True)                              # grid lines on
    # save to file
    out = "%s/%s-%s-%s-%s.pdf" % (figdir,op,x,y,no)
    plt.savefig(out, dpi=100, format="pdf", orientation="landscape")
    #plt.clf()
    #show()

def print_usage():
    print ("[USAGE] python %s <datadir> <figdir> <op> <xval> <yval> <extra> <nruns>" % str(sys.argv[0]))
    print ("where \t datadir is the path to the data files,")
    print ("\t figdir is the path where the plots will be saved,")
    print ("\t op in {seq, write},")
    print ("\t xval in {th, ws},")
    print ("\t yval in {bw, lat},")
    print ("\t extra is either the fixed number of threads or fixed block size used,")
    print ("\t nruns is the number of benchmark runs")
    sys.exit(2)

def main():
    # check if the number of input arguments is correct
    total = len (sys.argv)
    if total != 8:
        print_usage()

    # get input arguments
    datadir = sys.argv[1]   # relative path to data files 
    figdir = sys.argv[2]    # relative path to data files 
    op = sys.argv[3]        # operation: seq or write
    x = sys.argv[4]         # values on x axis: th or ws (threads or write sizes)
    y = sys.argv[5]         # values on y axis: bw or lat (bandwidth or latency)
    no = sys.argv[6]        # fixed number of threads or block size
    nruns = sys.argv[7]     # the number of benchmark runs
    # check correctness of input arguments
    if not (os.path.isdir(datadir) and os.path.exists(datadir)):
        print ("[ERROR] Choose valid path for data files!")
        print_usage()
    if not (os.path.isdir(figdir) and os.path.exists(figdir)):
        print ("[ERROR] Choose valid path for plot files!")
        print_usage()
    if (op != "seq" and op != "write"):
        print ("[ERROR] Choose operation: seq or write!")
        print_usage()
    if (x != "th" and x != "ws"):
        print ("[ERROR] Choose x value to plot: th or ws (threads or write size)!")
        print_usage()
    if (y != "bw" and y != "lat"):
        print ("[ERROR] Choose y value to plot: bw or lat (bandwidth or latency)!")
        print_usage()
    if not no.isdigit():
        print "[ERROR] Provide a number for the fixed benchmark parameter (# threads or block size)!"
        print_usage()
    if not nruns.isdigit():
        print "[ERROR] Provide a valid number of benchmark runs!"
        print_usage()
    # plot
    plot(datadir, figdir, op, x, y, no, nruns)

if __name__ == '__main__':
    main()
