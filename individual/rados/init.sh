#!/bin/bash

set -e

declare -a REP
declare -a SIZES
declare -a THREADS
declare -a SIZES_AUX
declare -a THREADS_AUX
declare -a NHOSTS
declare -a PGS

. ../util.sh

REP=(1 2 3)
SIZES_AUX=(4096)
#THREADS_AUX=(1 4 8 16)
THREADS_AUX=(16)

DURATION=100
nruns=5

ns=16       # object sizes to test with will be the powers of 2 up to 2^ns
nt=4        # number of threads to test with will be the powers of 2 up to 2^nt
# the number of OSDs in the cluster
nOSDs=`ceph status | grep osdmap | awk '{print $3}'`   
# the number of PGs if the replication level is 1
# nearest power of two for the nOSDs * 100 / rep
nPGs=$(pow 2 $(round $(log $((nOSDs * 100)) 2)))

SIZES=("`awk 'BEGIN{for (i=2;i<='$ns';i++) printf("%d ", 2^i);}'`")
THREADS=("`awk 'BEGIN{for (i=1;i<='$nt';i++) printf("%d ", 2^i);}'`")
NHOSTS=(`seq 1 10`) #(`seq 1 20`) # number of hosts for parallel rados
PGS=(`seq $((nOSDs*10)) $((nOSDs*10)) $((nOSDs*80))`) # from 260 to 2080, in increments of 260

outdir="out"
datdir="dat"
figdir="fig"
