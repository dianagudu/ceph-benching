#!/usr/bin/env python

import rados, sys
import time

sizes=[4, 8, 16, 32, 64, 128, 256, 512, 1024, \
       2048, 4096, 8192, 16384, 32768, 65536] #, \
       131072, 262144, 524288, 1048576] # in KB

MAX=65536
duration = 250

f = open("dat-2/librados-write-bw-ws-1-2rep.dat", "a")

with rados.Rados(conffile='/etc/ceph/ceph.conf') as cluster:
    with cluster.open_ioctx('bench-2') as ioctx:
            for sz in sizes:
                total_wr = duration * 1024 * 1024 / (sizes[0] ** 0.25) * (sz ** 0.25)
                total_mb = total_wr / ( 1024 * 1024 )

                size = sz * 1024  # in Bytes
                count = int(total_wr / size)

                if sz <= MAX:
                    data = '0' * size
                    start = time.clock()
                    for i in range(0, count):
                        obj = "obj_librados_bench_%s_%s" % (sz,i)
                        ioctx.write_full(obj, data)
                    end = time.clock()
                else:
                    data = '0' * MAX
                    start = time.clock()
                    for i in range(0, count):
                        obj = "obj_librados_bench_%s_%s" % (sz,i)
                        for j in range(0, size/MAX):
                            ioctx.write(obj, data, j*MAX)
                    end = time.clock()

                string = "%s %s\n" % (sz, total_mb / (end-start))
                f.write(string)

                print string
            f.close()
