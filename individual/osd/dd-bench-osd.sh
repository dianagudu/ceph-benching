#!/bin/bash

#########################################################################################
usage="$(basename "$0") [-h] [-o outdir] [-i hosts_osds] [-n nruns] [-d] -- program to benchmark the raw disk performance of OSDs
where:
    -h  show this help test
    -o  set the output directory (default: out-dd)
    -i  set the hostfile with an ordered list of hosts by OSD number (default: hosts_osd)
    -n  set the number of runs for the benchmark (default: 5)
    -d  use ceph commands to discover the osd hosts automatically and write them to hostfile
    
[NOTE] The benchmark assumes that all OSDs have the data stored on /dev/sdb1"

outdir="out-dd"
hostfile="hosts_osds"
nruns=5
discover=0

while getopts ':hdo:i:n:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        i)  hostfile=$OPTARG;;
        n)  nruns=$OPTARG;;
        d)  discover=1;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

#########################################################################################

# the number of OSDs in the ceph cluster
nOSDs=`ceph status | grep osdmap | awk '{print $3}'`   

# create the hostfile
if [[ $discover -eq 1 ]]; then
    ./discover-osds.sh $hostfile
fi

mkdir -p $outdir

for run in `seq 0 $((nruns-1))`; do
    pssh -h $hostfile -t 0 -i \
        'dir=`df -h | grep /dev/sdb1 | awk '"'"'{print $6}'"'"'`;\
         dd if=/dev/zero of=${dir}/here bs=4M count=250 oflag=direct;\
         rm -rf ${dir}/here' \
         2>& 1| tee ${outdir}/dd.$run.out
done

# nodes in hosts_osds are ordered by OSD number, 
# i.e. node7 is osd.0, node8 is osd.1 and so on

awk '/MB\/s/ { \
    a[FNR]+=$8;b[FNR]++; \
    } \
    END { \
        n = 0; \
        for (i=1;i<=FNR;i++) \
            if (b[i]!=0) {\
                printf("osd.%d %.2f MB/s\n", n, a[i]/b[i]); \
                n++; \
            }\
    }'  ${outdir}/dd.*.out > ${outdir}/dd.out
