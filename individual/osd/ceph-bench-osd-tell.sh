#!/bin/bash

#########################################################################################
usage="$(basename "$0") [-h] [-o outdir] [-i hosts_osds] [-n nruns] [-d] -- program to benchmark the individual performance of OSDs
where:
    -h  show this help test
    -o  set the output directory (default: out-osd)
    -n  set the number of runs for the benchmark (default: 5)"    

outdir="out-osd"
nruns=5

while getopts ':ho:n:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

#########################################################################################

mkdir -p $outdir

# the list of OSDs in the ceph cluster
OSDS=`ceph osd dump | egrep "osd.*up" | awk -F'[. ]' '{print $2}'`

# modify OSD limits
# ceph tell osd.* injectargs "--osd-bench-large-size-max-throughput 104857600"

for i in $OSDS; do
    # run the test several time per OSD
    for run in `seq 0 $((nruns-1))`; do
        ceph tell osd.$i bench  >&1| tee -a ${outdir}/osd.tell.$run.out
    done
done

# compute the average speed in MB/s per OSD
awk -F"\"" '/bytes_per_sec/ { \
    a[FNR]+=$4;b[FNR]++; \
    } \
    END { \
        n = 0; \
        for (i=1;i<=FNR;i++) \
            if (b[i]!=0) {\
                printf("osd.%d %.2f MB/s\n", n, a[i] / (b[i]*1024*1024) ); \
                n++; \
            }\
    }'  ${outdir}/osd.tell.*.out > ${outdir}/osd.tell.out
