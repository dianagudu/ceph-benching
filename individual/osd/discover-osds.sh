#!/bin/bash

hostfile=${1:-"hosts_osds"}

rm -rf $hostfile

# get a list of all osds that are up from ceph osd dump
for i in `ceph osd dump | egrep "osd.*up" | awk -F'[. ]' '{print $2}'`;
do
    # get location info about osd $i in JSON format and grep for host bucket
    host=$(ceph osd find $i | \
            grep -o '"host": ".*"' | \
            awk -F": " '{print $2}' | \
            sed 's/\(^"\|"$\)//g')
    # add host to the hosts file, including root username
    echo $host root >> $hostfile
done
