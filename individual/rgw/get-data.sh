#!/bin/bash

collect_data() {
    # get the necessary arguments
    op=$1     # read or write
    xval=$2   # th or ws
    yval=$3   # bw or lat
    # pattern to search for in test results
    if [ "$yval" = 'bw' ]; then
        pattern="Bandwidth (MB"
        yheader="bandwidth(MB/s)"
    else
        pattern="Average Latency"
        yheader="latency(s)"
    fi
    # performance for different #threads
    if [ "$xval" = 'th' ]; then
        for ws in ${SIZES_AUX[*]}; do
            outfile="$datdir/data-${op}-${yval}-${xval}-${ws}.dat"
            echo "# threads ${yheader}" > $outfile
            for th in ${THREADS[*]}; do 
                infile="${outdir}/rest_bench.${op}.${ws}KB.${th}th.log"
                grep "${pattern}" ${infile} | awk "{print \"${th}\", \$3 }" | tee -a ${outfile}
            done
        done
    else # performance for different block sizes
        for th in ${THREADS_AUX[*]}; do
            outfile="$datdir/data-${op}-${yval}-${xval}-${th}.dat"
            echo "# block_size ${yheader}" > $outfile
            for ws in ${SIZES[*]}; do 
                infile="${outdir}/rest_bench.${op}.${ws}KB.${th}th.log"
                grep "${pattern}" ${infile} | awk "{print \"${ws}\", \$3 }" | tee -a ${outfile}
            done
        done
    fi
}

get_parallel_data() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local op=${3:?no operation given}   # operation: write or seq
    local runno=$4

    datfile_bw=${datdir}/parallel${runno}-${op}-bw-${threads}-${size}.dat
    datfile_lat=${datdir}/parallel${runno}-${op}-lat-${threads}-${size}.dat
    suffix="${op}.${size}KB.${threads}th.log"
    
    for NH in ${NHOSTS[*]}; do
        bw_avg=0
        lat_avg=0
        for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rest)`; do
            outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
            bw_avg=`grep "Bandwidth (MB" ${outfile} | awk '{print '$bw_avg'+$3 }'`
            lat_avg=`grep "Average Latency" ${outfile} | awk '{print '$lat_avg'+$3 }'`
        done
        #bw_avg=$(awk 'BEGIN { print '$bw_avg'/'$NH' }')
        lat_avg=$(awk 'BEGIN { print '$lat_avg'/'$NH' }')
        echo $NH ${bw_avg} | tee -a ${datfile_bw}
        echo $NH ${lat_avg} | tee -a ${datfile_lat}
    done
}

rm -rf ${datdir}/*.dat

### init test scenario params
. init.sh

### get data for rest-bench
for op in 'write' 'seq'; do
    for xval in 'th' 'ws'; do
        for yval in 'bw' 'lat'; do
            collect_data $op $xval $yval
        done
    done
done

### get data for parallel rest-bench
for i in `seq 0 $((nruns-1))`; do
    get_parallel_data 4096 16 'write' $i
done
