#!/bin/bash

cleanup() {
    s3cmd rb --force `s3cmd ls | grep rest-bench-bucket | awk '{print $3}'`
}

rest_bench() {
    local size=${1:?no object size given}
    local threads=${2:-16}
    local size_bytes=$((size * 1024))
    local op=${3:?no operation given}
    local dur=$DURATION

    # add --no-cleanup if we are writing
    extra=""
    if [ "$op" = "write" ]; then 
        extra="--no-cleanup"
        dur=$((DURATION * 2))
    fi
    # clear chaches
    ./../sync.sh
    # run benchmark
    echo "# rest-bench size=$size threads=$threads op=$op"
    rest-bench --access-key='66U6UNQSO4R48O42F26X' \
      --secret='ndF5Owz8kFFLCnPdeO2c4V4f3hfDIdlCMZu3VJQZ' \
      --api-host=admin.smallcluster \
      --seconds $dur -b $size_bytes -t $threads $extra ${op} | \
      tee "$outdir/rest_bench.${op}.${size}KB.${threads}th.log"
}

parallel_rest() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local op=${3:?no operation given}   # operation: write or seq
    local runno=$4
    local dur=$DURATION

    # add --no-cleanup if we are writing
    extra=""
    if [ "$op" = "write" ]; then 
        extra="--no-cleanup"
        dur=$((DURATION * 2))
    fi

    # launch several parallel rest bench
    for NH in ${NHOSTS[*]}; do
        # clear chaches
        ./../sync.sh
        suffix="${op}.${size}KB.${threads}th.log"
        pssh -h <(head -${NH} hosts_parallel_rest) -t 0 -i \
           "rest-bench --access-key='66U6UNQSO4R48O42F26X' \
           --secret='ndF5Owz8kFFLCnPdeO2c4V4f3hfDIdlCMZu3VJQZ' \
           --api-host=admin.smallcluster \
           --bucket=rest-bench-bucket-`hostname` \
           --seconds $dur -b $size_bytes -t $threads $extra ${op} | \
           tee parallel${runno}-${NH}.${suffix}"
        # wait for them to finish
        sleep 5
    done
}

get_parallel_output() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local op=${3:?no operation given}   # operation: write or seq
    local runno=$4
    
    # gather results from remote machines to local host
    for NH in ${NHOSTS[*]}; do
        suffix="${op}.${size}KB.${threads}th.log"
        for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rest)`; do
            outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
            scp root@${host}:parallel${runno}-${NH}.${suffix} ${outfile}
            # remove output files on remote machines
            ssh root@${host} "rm parallel${runno}-${NH}.*"
        done
    done
}

rest_series() {
    # run tests with different number of threads, for fixed block size
    for th in ${THREADS[*]}; do
        for size in ${SIZES_AUX[*]}; do
            for op in 'write' 'seq'; do
                rest_bench $size $th $op
            done
            cleanup
        done
    done
    # run tests with different block sizes, for fixed number of threads
    for size in ${SIZES[*]}; do
        for th in ${THREADS_AUX[*]}; do
            for op in 'write' 'seq'; do
                rest_bench $size $th $op
            done
            cleanup
        done
    done
}

parallel_series() {
    size=4096
    th=16

    for i in `seq 0 $((nruns-1))`; do
        for op in 'write' 'seq'; do
            parallel_rest $size $th $op $i
            get_parallel_output $size $th $op $i
        done
        cleanup
    done
}

#### init test scenario
. init.sh

#### run serial tests
rest_series

#### run parallel tests
parallel_series

