#!/bin/bash

figdir='fig-1'
datdir='dat-1'
sizes='4096'
threads='16'

for op in 'write'; do # 'seq'; do
    for y in 'bw' 'lat'; do
        for s in $sizes; do
            python plot.py $datdir $figdir $op 'th' $y $s
        done
        for th in $threads; do
            python plot.py $datdir $figdir $op 'ws' $y $th
        done
    done
done


python plotpar.py $datdir $figdir 'write' 'bw' 16 4096 3
python plotpar.py $datdir $figdir 'write' 'lat' 16 4096 3

