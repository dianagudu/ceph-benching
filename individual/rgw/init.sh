#!/bin/bash

set -e

declare -a SIZES
declare -a THREADS
declare -a SIZES_AUX
declare -a THREADS_AUX
declare -a NHOSTS

THREADS=(2 4 8 10 16 20 24 25 26 27 30 32)
SIZES_AUX=(4096)
THREADS_AUX=(16)

ns=19       # object sizes to test with will be the powers of 2 up to 2^ns
nOSDs=26    # the number of OSDs in the cluster

SIZES=("`awk 'BEGIN{for (i=2;i<='$ns';i++) printf("%d ", 2^i);}'`")
#NHOSTS=(`seq 2 $((nOSDs+2))`) # number of hosts for parallel rados
NHOSTS=(`seq 2 10`) # number of hosts for parallel rados

DURATION=100
nruns=3

outdir=out-1
datdir=dat-1

mkdir -p $outdir
mkdir -p $datdir
