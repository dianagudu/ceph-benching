#!/bin/bash

round()
{
    ARG1=${1:?}
    echo $(( ${ARG1/.*} + 1 ))
}
 
log()
{
    ARG1=${1:?}
    ARG2=${2:?}
    echo "l($ARG1) / l($ARG2)" | bc -l
}
 
pow()
{
    ARG1=${1:?}
    ARG2=${2:?}
    echo $(( $ARG1 ** $ARG2 ))
}

