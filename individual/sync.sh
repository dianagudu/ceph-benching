#!/bin/bash

curr=`pwd`
cd $( dirname "${BASH_SOURCE[0]}" )

pssh -h pssh_all_hosts -i 'sudo echo 3 | sudo tee /proc/sys/vm/drop_caches && sudo sync'

cd $curr
