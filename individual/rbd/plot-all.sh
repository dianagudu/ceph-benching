#!/bin/bash

. init.sh

usage="$(basename "$0") [-h] [-d datdir] [-f figdir] [-n nruns] -b benchmark -- program to plot results for RBD performance

where:
    -h  show this help test
    -d  set the data directory where the data to be plotted are (default: dat)
    -f  set the figure directory where the plots will be saved (default: fig)
    -n  set the number of runs for the parallel benchmark (default: 5)
    -b  select the benchmark for which to process collected data (mandatory)

options for benchmarks: 
    rbd-bench       serial tests using the Ceph internal tool -- rbd bench
    rbd-ko          serial tests using the rbd kernel module, using dd and fio
    fio             serial tests using the modified version of fio, installed in folder ~/fio
    parallel-rbd    parallel tests using the Ceph internal tool -- rbd bench
    parallel-fio    parallel tests using the modified version of fio

Other parameters can be set in init.sh
"    
while getopts ':hd:f:n:b:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        b)  bench=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ -z "$bench" ]
then
    echo "$usage" >&2
    exit 1
fi

mkdir -p $figdir

## plot the selected benchmark
case "$bench" in
    rbd-bench)  
            for s in ${SIZES_AUX[*]}; do
                python plot.py $datdir $figdir 'th' $s
            done
            for th in ${THREADS_AUX[*]}; do
                python plot.py $datdir $figdir 'ws' $th
            done
            ;;
    rbd-ko)
            python plot-dd.py $dir $outfig 'ws' 1
            python plot-dd.py $dir $outfig 'th' 4096
            for s in ${SIZES_AUX[*]}; do
                python plot-fio.py $datdir $figdir 'th' $s 'true'
            done
            for th in ${THREADS_AUX[*]}; do
                python plot-fio.py $datdir $figdir 'ws' $th 'true'
            done
            ;;
    fio)        
            for s in ${SIZES_AUX[*]}; do
                python plot-fio.py $datdir $figdir 'th' $s 'false'
            done
            for th in ${THREADS_AUX[*]}; do
                python plot-fio.py $datdir $figdir 'ws' $th 'false'
            done
            ;;
    parallel-rbd)
            python plotpar.py $dir $outfig 16 4096 $nruns "rbd-bench"
            ;;
    parallel-fio)
            python plotpar-fio.py $dir $outfig 16 4096 $nruns
            ;;
    *)      
            printf "invalid benchmark name: -%s\n\n" "$bench" >&2
            echo "$usage" >&2
            ;;
esac



