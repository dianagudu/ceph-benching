#!/bin/bash

collect_rbd_bench_data() {
    xval=$1   # th or ws
    
    # performance for different #threads
    if [ "$xval" = 'th' ]; then
        for ws in ${SIZES_AUX[*]}; do
            outfile="$datdir/data-write-${xval}-${ws}.dat"
            echo "# threads bandwidth(MB/s)" > $outfile
            for th in ${THREADS[*]}; do 
                infile="${outdir}/rbd_bench.${ws}KB.${th}th.log"
                grep 'bytes/sec' ${infile} | awk '{print '${th}', $8/1048576 }' | tee -a ${outfile}
            done
        done
    else # performance for different block sizes
        for th in ${THREADS_AUX[*]}; do
            outfile="$datdir/data-write-${xval}-${th}.dat"
            echo "# block_size bandwidth(MB/s)" > $outfile
            for ws in ${SIZES[*]}; do 
                infile="${outdir}/rbd_bench.${ws}KB.${th}th.log"
                grep 'bytes/sec' ${infile} | awk '{print '${ws}', $8/1048576 }' | tee -a ${outfile}
            done
        done
    fi
}

collect_parallel_rbd_bench_data() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local runno=$3

    datfile_bw=${datdir}/parallel${runno}-rbd-bench-write-${threads}-${size}.dat
    suffix="rbd-bench-write.${size}KB.${threads}th.log"
    
    for NH in ${NHOSTS[*]}; do
        bw_avg=0
        for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
            outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
            bw_avg=`grep "bytes/sec" ${outfile} | awk '{print '$bw_avg'+$8/1048576 }'`
        done
        echo $NH ${bw_avg} | tee -a ${datfile_bw}
    done
}

get_all_parallel() {
    rm -rf ${datdir}/parallel*.dat
    for i in `seq 0 $((nruns-1))`; do
        collect_parallel_rbd_bench_data 4096 16 $i
    done
}

collect_dd_bench_data() {
    xval=$1         # th or ws

    # performance for different sizes, 1 thread
    if [ "$xval" = 'ws' ]; then
        outfile="$datdir/data_dd_write_${xval}-1.dat"
        echo "# block_size bandwidth(MB/s)" > $outfile
        for ws in ${SIZES[*]}; do 
            infile="${outdir}/dd_bench.${ws}KB.1th.log"
            grep 'B/s' ${infile} | awk '{ \
                if ($9 == "kB/s") print '${ws}', $8/1024; \
                else print '${ws}', $8; \
                }' | tee -a ${outfile}
        done
    else
        # fixed size used to write
        for size in ${SIZES_AUX[*]}; do
            outfile="$datdir/data_dd_write_${xval}-${size}.dat"
            echo "# threads bandwidth(MB/s)" > $outfile
            for th in ${THREADS[*]}; do
                prefix="$outdir/dd_bench.${size}KB.${th}th"
                bw_avg=0
                for i in `seq 1 ${th}`; do
                    bw_avg=`grep 'B/s' ${prefix}.${i}.log | awk '{ \
                        if ($9 == "kB/s") print '${bw_avg}'+$8/1024; \
                        else print '${bw_avg}'+$8; \
                        }'`
                done
                echo $th $bw_avg | tee -a ${outfile}
            done
        done
    fi
}

collect_fio_data() {
    xval=$1 # th or ws
    op=$2   #  one of the ops in FIO_OPS
    
    # performance for different #threads
    if [ "$xval" = 'th' ]; then
        for ws in ${SIZES_AUX[*]}; do
            outfile="$datdir/fio-${op}-${xval}-${ws}.dat"
            echo "# threads bandwidth(MB/s)" > $outfile

            for th in ${THREADS[*]}; do 
                intfile=${outdir}/fio_bench.${op}.${ws}KB.${th}th.log

                grep 'aggrb' ${infile} | awk -F '=|,' '{\
                    unit=substr($4,length($4)-3);\
                    bw=substr($4,0,length($4)-4);\
                    if (unit=="KB/s") print '${th}', (bw/1024);\
                    else print '${th}', bw}' \
                    | tee -a ${outfile}
            done
        done
    else # performance for different block sizes
        for th in ${THREADS_AUX[*]}; do
            outfile="$datdir/fio-${op}-${xval}-${th}.dat"
            echo "# block_size bandwidth(MB/s)" > $outfile

            for ws in ${SIZES[*]}; do 
                infile="${outdir}/fio_bench.${op}.${ws}KB.${th}th.log"

                grep 'aggrb' ${infile} | awk -F '=|,' '{\
                    unit=substr($4,length($4)-3);\
                    bw=substr($4,0,length($4)-4);\
                    if (unit=="KB/s") print '${ws}', (bw/1024);\
                    else print '${ws}', bw}' \
                    | tee -a ${outfile}
            done
        done
    fi
}

collect_fio_rbd_data() {
    xval=$1 # th or ws
    op=$2   #  one of the ops in FIO_OPS
    
    # performance for different #threads
    if [ "$xval" = 'th' ]; then
        for ws in ${SIZES_AUX[*]}; do
            outfile_rbd="$datdir/fio-rbd-${op}-${xval}-${ws}.dat"
            echo "# threads bandwidth(MB/s)" > $outfile_rbd

            for th in ${THREADS[*]}; do 
                intfile_rbd=${outdir}/fio_rbd_bench.${op}.${ws}KB.${th}th.log

                grep 'aggrb' ${infile_rbd} | awk -F '=|,' '{\
                    unit=substr($4,length($4)-3);\
                    bw=substr($4,0,length($4)-4);\
                    if (unit=="KB/s") print '${th}', bw/1024;\
                    else print '${th}', bw}' \
                    | tee -a ${outfile_rbd}
            done
        done
    else # performance for different block sizes
        for th in ${THREADS_AUX[*]}; do
            outfile_rbd="$datdir/fio-rbd-${op}-${xval}-${th}.dat"
            echo "# block_size bandwidth(MB/s)" > $outfile_rbd

            for ws in ${SIZES[*]}; do 
                infile_rbd="${outdir}/fio_rbd_bench.${op}.${ws}KB.${th}th.log"

                grep 'aggrb' ${infile_rbd} | awk -F '=|,' '{\
                    unit=substr($4,length($4)-3);\
                    bw=substr($4,0,length($4)-4);\
                    if (unit=="KB/s") print '${ws}', bw/1024;\
                    else print '${ws}', bw}' \
                    | tee -a ${outfile_rbd}
            done
        done
    fi
}

collect_parallel_fio_bench_data() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local runno=$3
    local op=$4

    datfile_bw=${datdir}/parallel${runno}-fio-${op}-${threads}-${size}.dat
    suffix="fio-rbd.${op}.${size}KB.${threads}th.log"
    
    for NH in ${NHOSTS[*]}; do
        bw_avg=0
        for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
            outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
            bw_avg=`grep 'aggrb' ${outfile} | awk -F '=|,' '{\
                    unit=substr($4,length($4)-3);\
                    bw=substr($4,0,length($4)-4);\
                    if (unit=="KB/s") print '${bw_avg}' + (bw/1024);\
                    else print '${bw_avg}' + bw}'`
        done
        echo $NH ${bw_avg} | tee -a ${datfile_bw}
    done
}

get_all_parallel_fio() {
    rm -rf ${datdir}/parallel*-fio-*.dat
    for i in `seq 0 $((nruns-1))`; do
        for op in ${FIO_OPS[*]}; do
            collect_parallel_fio_bench_data 4096 16 $i $op
        done
    done
}

. init.sh

#########################################################################################

usage="$(basename "$0") [-h] [-o outdir] [-d datdir] [-n nruns] -b benchmark -- program to process performance data for RBD

where:
    -h  show this help test
    -o  set the output directory where data were collected (default: out)
    -d  set the data directory (default: out)
    -n  set the number of runs for the parallel benchmark (default: 5)
    -b  select the benchmark for which to process collected data (mandatory)

options for benchmarks: 
    rbd-bench       serial tests using the Ceph internal tool -- rbd bench
    rbd-ko          serial tests using the rbd kernel module, using dd and fio
    fio             serial tests using the modified version of fio, installed in folder ~/fio
    parallel-rbd    parallel tests using the Ceph internal tool -- rbd bench
    parallel-fio    parallel tests using the modified version of fio

Other parameters can be set in init.sh
"    
while getopts ':ho:d:n:b:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        b)  bench=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ -z "$bench" ]
then
    echo "$usage" >&2
    exit 1
fi

#########################################################################################

mkdir -p $datdir

## process the selected benchmark data
case "$bench" in
    rbd-bench)  
            collect_rbd_bench_data 'th'
            collect_rbd_bench_data 'ws'
            ;;
    rbd-ko)
            collect_dd_bench_data 'ws'
            collect_dd_bench_data 'th'
            for op in ${FIO_OPS[*]}; do
                collect_fio_data 'ws' $op
                collect_fio_data 'th' $op
            done
            ;;
    fio)        
            for op in ${FIO_OPS[*]}; do
                collect_fio_rbd_data 'ws' $op
                collect_fio_rbd_data 'th' $op
            done
            ;;
    parallel-rbd)
            get_all_parallel
            ;;
    parallel-fio)
            get_all_parallel_fio
            ;;
    *)      
            printf "invalid benchmark name: -%s\n\n" "$bench" >&2
            echo "$usage" >&2
            ;;
esac
