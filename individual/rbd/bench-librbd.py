#!/usr/bin/env python

import rados
import rbd
import time

sizes=[4, 8, 16, 32, 64, 128, 256, 512, 1024, \
       2048, 4096, 8192, 16384, 32768, 65536, \
       131072, 262144, 524288, 1048576] # in KB

total_sz = 20 * 1024**3 # 20 GB
total_wr = 1024**3 # 1 GB
total_mb = 1024

f = open("dat-1/data-librbd-ws-1.dat", "w")

with rados.Rados(conffile='/etc/ceph/ceph.conf') as cluster:
    with cluster.open_ioctx('rbd') as ioctx:
        rbd_inst = rbd.RBD()
        rbd_inst.create(ioctx, 'myimage', total_sz)
        with rbd.Image(ioctx, 'myimage') as image:
            for sz in sizes:
                size = sz * 1024  # in Bytes
                data = '0' * size
                count = total_wr / size

                start = time.clock()
                for i in range(0, count):
                    image.write(data, i * size)
                end = time.clock()

                string = "%s %s\n" % (sz, total_mb / (end-start))
                f.write(string)
            f.close()
        rbd_inst.remove(ioctx,'myimage')
