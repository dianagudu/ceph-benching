#!/bin/bash

##############################################################
# functions for creating/destroying rbd images
##############################################################

create_rbd_img() {
    rbd create --size $1 $2
}

rm_rbd_img() {
    rbd rm $1
}

create_rbd_ko() {
    # create volume
    rbd create --size 40960 ${VOL2}
    # map the device
    rbd map ${VOL2}
    # format it
    mkfs.ext4 /dev/rbd1
    # mount it
    mount /dev/rbd1 /mnt
}

destroy_rbd_ko() {
    # unmount
    umount /mnt
    # unmap device
    rbd unmap /dev/rbd1
    # delete volume
    rbd rm ${VOL2}
}

##############################################################
# functions for running individual benchmarks
##############################################################

rbd_bench() {
    local size=${1:?no object size given}
    local threads=${2:-16}
    local vol=$3
    local size_bytes=$((size * 1024))
    # scale io-total with block size to keep run times in bounds
    # we scale down by the 4th root
    local total=$(python <<_EOT_
coeff = $DURATION * 10 * 1024 * 1024 / (4 ** 0.25)
total = coeff * (${size} ** 0.25)
print(total)
_EOT_
)
    echo "# rbd bench-write size=$size threads=$threads"
    rbd bench-write $vol --io-size $size_bytes --io-threads $threads --io-total ${total%.*} | \
      tee "$outdir/rbd_bench.${size}KB.${threads}th.log"
}

parallel_rbd() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local runno=$3
    local NH=$4

    local total=$(python <<_EOT_
coeff = $DURATION * 10 * 1024 * 1024 / (4 ** 0.25)
total = coeff * (${size} ** 0.25)
print(total)
_EOT_
)

    # clear chaches
    ./../sync.sh

    suffix="rbd-bench-write.${size}KB.${threads}th.log"

    # launch several parallel rbd bench
    pssh -h <(head -${NH} hosts_parallel_rbd) -t 0 -i \
        "rbd bench-write volrbd-\`hostname\` --io-size $size_bytes --io-threads $threads --io-total ${total%.*} | \
            tee parallel${runno}-${NH}.${suffix}"
    
    # wait for them to finish
    sleep 5

    # gather results from remote machines to local host
    for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
        outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
        scp root@${host}:parallel${runno}-${NH}.${suffix} ${outfile}
        # remove output files on remote machines
        ssh root@${host} "rm parallel${runno}-${NH}.*"
    done
}

dd_bench() {
    local size=${1:?no object size given}
    local threads=${2:-1}
    local count=$(python <<_EOT_
coeff = $DURATION * 10 * 1024 / (${SIZES[0]} ** 0.25)
total = coeff * (${size} ** 0.25)
print(int(total / ${size}))
_EOT_
)

    if [ $threads -eq 1 ]; then
        dd if=/dev/zero of=/mnt/f${size} bs=${size}K count=${count} \
          oflag=direct 2>&1 | tee "$outdir/dd_bench.${size}KB.1th.log"
        rm -rf /mnt/f${size}
        # dd local
        dd if=/dev/zero of=f${size} bs=${size}K count=${count} \
          oflag=direct 2>&1 | tee "$outdir/dd_bench.local.${size}KB.1th.log"
        rm -rf f${size}
    else
        for i in `seq $threads`; do
            { dd if=/dev/zero of=/mnt/f${size}.${threads}.${i} bs=${size}K count=${count} \
              oflag=direct 2>&1 | tee "$outdir/dd_bench.${size}KB.${threads}th.${i}.log"; } &
        done
        wait
        rm -rf /mnt/f${size}.${threads}.*
    fi
}

bonnie_bench() {
    echo "Bonnie++ bench not implemented yet"
}

fio_bench() {
    size=$1
    threads=$2
    op=$3
    logdir=$4
    logfile=$5

    outfile=${outdir}/fio_bench.${op}.${size}KB.${threads}th.log
    mkdir -p $logdir

    DISK=/mnt/fio.${size}.${threads} LOGFILE=${logfile} \
      THREADS=${threads} BS=${size}k SIZE=1g \
      fio --output ${outfile} --section ${op} all.fio
    
    if [[ $op =~ "*-read" ]]; then
        rm -rf /mnt/fio.${size}.${threads}
    fi

    mv ${logfile}* ${logdir}/
}

fio_rbd_bench() {
    size=$1
    threads=$2
    op=$3
    logdir=$4
    logfile=$5

    outfile=${outdir}/fio_rbd_bench.${op}.${size}KB.${threads}th.log
    mkdir -p $logdir

    LOGFILE=${logfile} THREADS=${threads} BS=${size}k VOL="fio_test" \
      ~/fio/fio --output ${outfile} --section ${op} rbd.fio

    mv ${logfile}* ${logdir}/
}

parallel_fio_rbd() {
    local size=$1                       # optimal write size, in KB
    local size_bytes=$((size * 1024))   # convert size to bytes
    local threads=$2                    # optimal number of threads
    local op=$3
    local logfile=$4
    local runno=$5
    local NH=$6

    # clear chaches
    ./../sync.sh

    suffix="fio-rbd.${op}.${size}KB.${threads}th.log"

    # launch several parallel rbd bench
    pssh -h <(head -${NH} hosts_parallel_rbd) -t 0 -i \
      "LOGFILE=${logfile} THREADS=${threads} BS=${size}k \
       VOL=volrbd-\`hostname\` ~/fio/fio --output  \
       parallel${runno}-${NH}.${suffix} --section ${op} rbd.fio"
    
    # wait for them to finish
    sleep 5

    # gather results from remote machines to local host
    for host in `awk '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
        outfile=${outdir}/parallel${runno}-${NH}.${host}.${suffix}
        scp root@${host}:parallel${runno}-${NH}.${suffix} ${outfile}
        # remove output files on remote machines
        ssh root@${host} "rm parallel${runno}-${NH}.*"
    done
}

##############################################################
# functions for running series of individual benchmarks
##############################################################

rbd_series() {
    for size in ${SIZES[*]}; do
        for th in ${THREADS_AUX[*]}; do
            rbd_bench $size $th $VOL1
        done
    done
    for th in ${THREADS[*]}; do
        for size in ${SIZES_AUX[*]}; do
            rbd_bench $size $th $VOL1
        done
    done
}

rbd_parallel_series() {
    # values for best performance
    size=4096
    th=16

    for i in `seq 0 $((nruns-1))`; do
        for NH in ${NHOSTS[*]}; do
            for host in `awk -F'.' '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
                create_rbd_img 10240 volrbd-${host}
            done
            parallel_rbd $size $th $i $NH
            for host in `awk -F'.' '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
                rm_rbd_img volrbd-${host}
            done
        done
    done
}

dd_series() {
    for size in ${SIZES[*]}; do
        dd_bench $size 1 
    done
    for threads in ${THREADS[*]}; do
        dd_bench 4096 $threads
    done
}

bonnie_series() {
    for size in ${SIZES[*]}; do
        bonnie_bench $size
    done
}

fio_series() {
    rbd -p rbd create --size 1024 fio_test

    for size in ${SIZES[*]}; do
        for th in ${THREADS_AUX[*]}; do
            for op in ${FIO_OPS}; do
                logdir=${outdir}/fio-sizes-${op}-${th}
                fio_bench $size $th $op ${logdir} fio-${size}
            done
        done
    done
    for th in ${THREADS[*]}; do
        for size in ${SIZES_AUX[*]}; do
            for op in ${FIO_OPS}; do
                logdir=${outdir}/fio-threads-${op}-${size}
                fio_bench $size $th $op ${logdir} fio-${th}
            done
        done
    done
}

fio_rbd_series() {
    rbd -p rbd create --size 1024 fio_test

    for size in ${SIZES[*]}; do
        for th in ${THREADS_AUX[*]}; do
            for op in ${FIO_OPS}; do
                logdir_rbd=${outdir}/fio-rbd-sizes-${op}-${th}
                fio_rbd_bench $size $th $op ${logdir_rbd} fio-rbd-${size}
            done
        done
    done
    for th in ${THREADS[*]}; do
        for size in ${SIZES_AUX[*]}; do
            for op in ${FIO_OPS}; do
                logdir_rbd=${outdir}/fio-rbd-threads-${op}-${size}
                fio_rbd_bench $size $th $op ${logdir_rbd} fio-rbd-${th}
            done
        done
    done
}

fio_rbd_parallel_series() {
    size=4096
    th=16
    
    for host in `awk -F'.' '{print $1}' hosts_parallel_rbd`; do
        scp rbd.fio root@$host:
    done

    for i in `seq 0 $((nruns-1))`; do
        for NH in ${NHOSTS[*]}; do
            for host in `awk -F'.' '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
                create_rbd_img 10240 volrbd-${host}
            done

            for op in ${FIO_OPS}; do
                parallel_fio_rbd $size $th $op parallel-fio-rbd-$NH $i $NH
            done

            for host in `awk -F'.' '{print $1}' <(head -${NH} hosts_parallel_rbd)`; do
                rm_rbd_img volrbd-${host}
            done
        done
    done
}

## init test scenario params
. init.sh

#########################################################################################

usage="$(basename "$0") [-h] [-o outdir] [-n nruns] -b benchmark -- program to benchmark the RBD performance

where:
    -h  show this help test
    -o  set the output directory (default: out)
    -n  set the number of runs for the parallel benchmark (default: 5)
    -b  select the benchmark to be run (mandatory)

options for benchmarks: 
    rbd-bench       serial tests using the Ceph internal tool -- rbd bench
    rbd-ko          serial tests using the rbd kernel module, using dd and fio
    fio             serial tests using the modified version of fio, installed in folder ~/fio
    parallel-rbd    parallel tests using the Ceph internal tool -- rbd bench
    parallel-fio    parallel tests using the modified version of fio

Other parameters can be set in init.sh
"    
while getopts ':ho:n:b:' option; do
    case "$option" in
        h)  echo "$usage"
            exit
            ;;
        o)  outdir=$OPTARG;;
        n)  nruns=$OPTARG;;
        b)  bench=$OPTARG;;
        :)  printf "missing argument for -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
        \?) printf "illegal option: -%s\n\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ -z "$bench" ]
then
    echo "$usage" >&2
    exit 1
fi

#########################################################################################

mkdir -p $outdir

## create rbd pool with proper # PGs
ceph osd pool create rbd $nPGs $nPGs
ceph osd pool set rbd size 1

## run the selected benchmark
case "$bench" in
    rbd-bench)  
            # create volume
            create_rbd_img 10240 ${VOL1}
            # run tests
            rbd_series
            # delete volume
            rm_rbd_img ${VOL1}
            ;;
    rbd-ko)
            # create, map and mount volume
            create_rbd_ko
            # run tests
            dd_series
            #bonnie_series
            fio_series
            # unmount, unmap and delete volume
            destroy_rbd_ko
            ;;
    fio)        
            fio_rbd_series
            ;;
    parallel-rbd)
            rbd_parallel_series
            ;;
    parallel-fio)
            fio_rbd_parallel_series
            ;;
    *)      
            printf "invalid benchmark name: -%s\n\n" "$bench" >&2
            echo "$usage" >&2
            ;;
esac

## delete rbd pool
ceph osd pool delete rbd rbd --yes-i-really-really-mean-it 
