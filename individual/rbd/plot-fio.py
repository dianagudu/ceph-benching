#!/usr/bin/env python

import numpy as np
import pylab
from pylab import *
import os

ops=['seq-read', 'seq-write', 'rand-read', 'rand-write']

def plot(datadir, figdir, x, extra, rbd_ko):
    # set figure size
    fig = plt.figure(figsize=(10,8))
    if rbd_ko == 'true':
        prefix = 'fio'
    else:
        prefix = 'fio-rbd'
    # load data from file
    for op in ops:
        file = "%s/%s-%s-%s-%s.dat" %(datadir,prefix,op,x,extra)
        data = pylab.loadtxt(file)
        pylab.plot( data[:,0], data[:,1], label=op)

    # set title and x label depending on input
    if x == 'th':
        my_xlabel = '# threads'
        my_title = "RBD fio performance with many writers,\n %s KB block size" % extra
    else:
        my_xlabel = 'write size (KB)'
        my_title = "RBD fio performance with different sizes, \n %s threads" % extra
        xscale('log', basex=2) # use logarithmic scale

    my_ylabel = 'bandwidth (MB/s)'

    xticks(data[:,0])

    fontsize=18
    title(my_title, fontsize=fontsize*1.1)
    xlabel(my_xlabel, fontsize=fontsize)
    ylabel(my_ylabel, fontsize=fontsize)
    legend(loc='upper right')
    grid(True)

    # save to file
    out = "%s/%s-%s-%s.pdf" % (figdir,prefix,x,extra)
    plt.savefig(out, dpi=90, format="pdf", orientation="landscape")
    #show()

def print_usage():
    print ("[USAGE] python %s <datadir> <figdir> <xval> <extra> <rbd ko>" % str(sys.argv[0]))
    print ("where \t datadir is the path to the data files,")
    print ("\t figdir is the path where the plots will be saved,")
    print ("\t xval in {th, ws},")
    print ("\t extra is the fixed parameter, either number of threads or block size,")
    print ("\t rbd ko is either true or false, depending if the rbd kernel module was used for testing")
    sys.exit(2)

def main():
    # check if the number of input arguments is correct
    total = len (sys.argv)
    if total != 6:
        print_usage()

    # get input arguments
    datadir = sys.argv[1]   # relative path to data files 
    figdir = sys.argv[2]    # relative path to data files 
    x = sys.argv[3]         # values on x axis: th or ws (threads or write sizes)
    extra = sys.argv[4]     # fixed parameter (number of threads or write size)
    rbd_ko = sys.argv[5]    # whether rbd ko was used

    # check correctness of input arguments
    if not (os.path.isdir(datadir) and os.path.exists(datadir)):
        print ("[ERROR] Choose valid path for data files!")
        print_usage()
    if not (os.path.isdir(figdir) and os.path.exists(figdir)):
        print ("[ERROR] Choose valid path for plot files!")
        print_usage()
    if (x != "th" and x != "ws"):
        print ("[ERROR] Choose x value to plot: th or ws (threads or write size)!")
        print_usage()
    if not extra.isdigit():
        print ("[ERROR] Choose a valid extra parameter (number of threads or block size)!")
        print_usage()
    if (x != "true" and x != "false"):
        print ("[ERROR] Specify whether the RBD kernel module was used for testing (true or false)!")
        print_usage()
    # plot
    plot(datadir, figdir, x, extra, rbd_ko)

if __name__ == '__main__':
    main()
