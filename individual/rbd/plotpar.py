#!/usr/bin/env python

import numpy as np
import pylab
from pylab import *
import os

def plot(datadir, figdir, th, size, runs, work):
    # set figure size
    fig = plt.figure(figsize=(10,8))

    data = []
    for i in range(0, int(runs)):
        file = "%s/parallel%s-%s-write-%s-%s.dat" %(datadir,i,work,th,size)
        # load data from files
        data.append(pylab.loadtxt(file))

    # x values
    x = data[0][:,0]
    # compute average of runs
    avg = np.mean(data[:], axis=0)[:,1]
    # compute standard deviation
    stdev = np.std(data[:], axis=0)[:,1]
    # plot
    pylab.plot( x, avg, label='bw', marker='o')
    # plot error bars
    pylab.errorbar( x, avg, yerr=stdev, linestyle="None", marker="None")
    # different settings depending on input
    metric='Bandwidth'
    my_ylabel = 'aggregated bandwidth (MB/s)'
    #pylab.ylim([0,1000])
    #yticks([20*i for i in range(0,13)])

    my_xlabel='# clients'
    my_title = "%s for parallel %s write\n(%s threads each, %s KB block size)" \
            %(metric,work,th,size)

    fontsize=18
    title(my_title, fontsize=fontsize*1.1)  # figure title
    xlabel(my_xlabel, fontsize=fontsize)    # x asis labe;
    ylabel(my_ylabel, fontsize=fontsize)    # y axis label
    grid(True)                              # grid lines on
    xticks([i for i in range(0,12)])
    legend(loc='lower right')               # legend position
    # save to file
    out = "%s/parallel-%s-%s-%s.pdf" % (figdir,work,th,size)
    plt.savefig(out, dpi=100, format="pdf", orientation="landscape")
    #plt.clf()
    #show()

def print_usage():
    print ("[USAGE] python %s <datadir> <figdir> <threads> <size> <runs> <work>" % str(sys.argv[0]))
    print ("where \t datadir is the path to the data files,")
    print ("\t figdir is the path where the plots will be saved,")
    print ("\t threads is the fixed number of threads used on each host,")
    print ("\t size is the fixed block size used,")
    print ("\t runs is the number of runs,")
    print ("\t work is the type of benchmark run {rbd-bench}")
    sys.exit(2)

def main():
    # check if the number of input arguments is correct
    total = len (sys.argv)
    if total != 7:
        print_usage()

    # get input arguments
    datadir = sys.argv[1]   # relative path to data files 
    figdir = sys.argv[2]    # relative path to data files 
    th = sys.argv[3]        # fixed number of threads
    size = sys.argv[4]      # fixed block size
    runs = sys.argv[5]      # number of runs to get average of
    work = sys.argv[6]
    # check correctness of input arguments
    if not (os.path.isdir(datadir) and os.path.exists(datadir)):
        print ("[ERROR] Choose valid path for data files!")
        print_usage()
    if not (os.path.isdir(figdir) and os.path.exists(figdir)):
        print ("[ERROR] Choose valid path for plot files!")
        print_usage()
    if not th.isdigit():
        print "[ERROR] Provide a valid number of threads!"
        print_usage()
    if not size.isdigit():
        print "[ERROR] Provide a valid block size!"
        print_usage()
    if not runs.isdigit():
        print "[ERROR] Provide a valid number of runs!"
        print_usage()
    if (work != "rbd-bench"):
        print "[ERROR] Provide a valid benchmark type"
        print_usage()
    # plot
    plot(datadir, figdir, th, size, runs, work)

if __name__ == '__main__':
    main()
