#!/bin/bash

set -e

declare -a SIZES
declare -a THREADS
declare -a SIZES_AUX
declare -a THREADS_AUX
declare -a FIO_OPS

. ../util.sh

THREADS=(2 4 8 16 18 20 22 24 25 26 27 28 30 32)
SIZES_AUX=(4096 4)
#THREADS_AUX=(1 16)
THREADS_AUX=(16)
FIO_OPS='seq-write seq-read rand-write rand-read'

ns=16       # object sizes to test with will be the powers of 2 up to 2^ns
SIZES=("`awk 'BEGIN{for (i=2;i<='$ns';i++) printf("%d ", 2^i);}'`")

# the number of OSDs in the cluster
nOSDs=`ceph status | grep osdmap | awk '{print $3}'`   
nPGs=$(pow 2 $(round $(log $((nOSDs * 100)) 2)))
NHOSTS=(`seq 1 10`)   # number of hosts for parallel rbd

nruns=5
DURATION=25

VOL1=volrbd-1
VOL2=volrbd-2

outdir="out"
datdir="dat"
figdir="fig"
